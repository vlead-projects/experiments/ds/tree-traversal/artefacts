		#+TITLE: 
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Data required for generating random graph
** Array of JSON objects for rendering
 + Each element of array is a JSON Objects with field name which is the name of the nodes and children is a list of JSON objects.
 + Different Graphs are stored here in their Tree Structure as Parent,Children.
 + Treedata is an array of JSON objects for each tree structure.

#+NAME: data
#+BEGIN_SRC js

var treeData = [ {
	"name":"7",
	children:[  
	{ "name":"6"  , children:[{ "name":"4"},{ "name":"3"}]},
	{   "name":"5",children:[{ "name":"2"},{ "name":"1"}]},      
    ]
       }   ,
	{
	"name":"2",
	children:[  
	{ "name":"7"  , children:[{ "name":"2",children:[{"name":"8"},{"name":"11"}]},{ "name":"6"}]},
	{   "name":"5",children:[ {"name":"3"},{ "name":"9",children:[{"name":"4"},{"name":"18"}]}]},      
	]
	       }
	,
	{
	"name":"2",
	children:[  
	{ "name":"7"  , children:[  { "name":"21",children:[{"name":"8"},{"name":"11"}]},{ "name":"6"}  ]},
	{   "name":"5"},      
	]       },
	{
	"name":"2",
	children:[  
	{ "name":"7" ,children:[ { "name":"12"},{"name":"8"} ]},
	{   "name":"5",children:[ {"name":"3"},{ "name":"9",children:[{"name":"4"},{"name":"18"}]}]},      
	]       
	}
];

#+END_SRC

* Tangle                                      
#+BEGIN_SRC js :tangle data.js :eval no :noweb yes
<<data>>
#+END_SRC
